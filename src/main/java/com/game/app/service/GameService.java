package com.game.app.service;

import java.util.List;
import java.util.Map;

public interface GameService {

	/**
	 * 根据当前游戏步骤获取游戏的原图以及拼图的url列表
	 * 
	 * @param step
	 * @return
	 */
	public String getPictrueByStep(int raceStep, int rankStep, int mode);

	/**
	 * 录入当前用户积分
	 * 
	 * @param scoreData
	 *            用户积分信息 包含 那一句
	 * @return
	 */
	public boolean insertScore(Map<String, Object> scoreData);

	/**
	 * 获取不同模式的游戏步骤配置数据
	 * 
	 * @return
	 */

	List<Map<String, Object>> gameRankConfig();

}
