package com.game.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.game.app.dao.GameDao;
import com.game.app.dao.GamePictureDao;
import com.game.app.service.GameService;

@Service
public class UserServiceImpl implements GameService {

	private GamePictureDao gamePictureDao = new GamePictureDao();
	private GameDao GameDao = new GameDao();

	@Override
	public String getPictrueByStep(int raceStep, int rankStep, int mode) {
		// TODO Auto-generated method stub
		Random rand = new Random();
		List<String> picList = this.getPictureList(raceStep, rankStep, mode);
		int randomValue = rand.nextInt(picList.size());
		return picList.get(randomValue);

	}

	/**
	 * 根据当前游戏级别，当前游戏步骤获取游戏需要的原图以及小图
	 * 
	 * @param raceStep
	 * @param rankStep
	 * @return
	 */
	public List<String> getPictureList(int raceStep, int rankStep, int mode) {
		// TODO Auto-generated method stub
		List<String> picList = gamePictureDao.selectPictruesByStep(raceStep, rankStep, mode);
		return picList;
	}

	@Override
	public boolean insertScore(Map<String, Object> recordData) {
		// TODO Auto-generated method stub
		GameDao.insertScore(recordData);
		return true;
	}

	@Override
	public List<Map<String, Object>> gameRankConfig() {
		// TODO Auto-generated method stub
		return this.gamePictureDao.selectGameRankByMode();
	}

}
