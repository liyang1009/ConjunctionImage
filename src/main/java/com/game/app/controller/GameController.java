package com.game.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.game.app.service.GameService;

@RestController
@EnableAutoConfiguration
@RequestMapping("/game")
public class GameController {

	@Autowired
	public GameService gameService;

	/**
	 * 记录游戏得分
	 * 
	 * @param valueOne
	 * @return
	 */
	@RequestMapping(value = "/recordRaceScore", method = RequestMethod.POST)
	public String recordRaceScore(@RequestParam("raceStep") int raceStep, @RequestParam("score") float score,
			@RequestParam("rankStep") int rankStep, @RequestParam("gameMode") int gameMode, HttpSession session) {
		// 获取游戏得分数据
		Map<String, Object> paramMap = new HashMap<>();
		String uidString = "liyang";
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date());
		paramMap.put("raceStep", raceStep);
		paramMap.put("score", Math.round(score) * 100 / 100);
		paramMap.put("rankStep", rankStep);
		paramMap.put("userId", uidString);
		paramMap.put("gametime", timeStamp);
		paramMap.put("gameMode", gameMode);
		gameService.insertScore(paramMap);
		return "0";
	}

	/**
	 * 根据关卡获取对应的原图以及拼图小图片 需要后台进行图片上传切分
	 * 
	 * @param int
	 *            当前关卡
	 * @return
	 */
	@RequestMapping("/picture")
	public String givePictureConjuntion(@RequestParam("raceStep") int raceStep, @RequestParam("rankStep") int rankStep,
			@RequestParam("mode") int mode) {

		/*
		 * format
		 * {splited_order_mess:origin_image_url,splited_img_url,splited_img_url,
		 * splited_img_url] [splited_order_neat]
		 */
		String pic = this.gameService.getPictrueByStep(raceStep, rankStep, mode);
		return pic;
	}

	/**
	 * 根据游戏模式获取当前游戏的关卡以及步骤
	 * 
	 * @return
	 */
	@RequestMapping("/gameRankConfig")
	public List<Map<String, Object>> gameRankConfig() {

		return gameService.gameRankConfig();

	}

}
