package com.game.app.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.game.app.service.WeChatService;

/**
 * 用于对接微信相关业务逻辑 包括各种认证API，回调函数 等等。
 * 
 * @author liyang
 *
 */
@RestController
@RequestMapping("/wechat")
public class WeChatController {

	@Autowired
	private WeChatService service;

	@RequestMapping("/login")
	public Map<String, Object> getUid(@RequestParam("jscode") String jscode, HttpSession session) {

		Map<String, Object> result = new HashMap<String, Object>();
		if (session.getAttribute("uid") == null) {
			String uid = service.loginGetUid();
			if (uid != "") {
				session.setAttribute("uid", uid);
			}
		}
		result.put("code", 0);
		result.put("uid", session.getAttribute("uid"));
		return result;
	}

}
