package com.game.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {

	/**
	 * 根据key获取classpath下指定的名称的properties里面包含的数据
	 * 
	 * @param name
	 * @return String
	 */
	public static String getString(String name) {

		Properties prop = new Properties();
		InputStream inputStream = null;
		String value = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			inputStream = classLoader.getResourceAsStream("config.properties");
			prop.load(inputStream);
			value = prop.getProperty(name);
			System.out.println(value);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return value;
	}
}
