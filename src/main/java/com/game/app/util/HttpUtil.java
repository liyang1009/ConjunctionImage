package com.game.app.util;

import java.io.IOException;
import java.util.Map;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

/**
 * http请求工具类
 * 
 * @author liyang
 *
 */
public class HttpUtil {

	/**
	 * 对指定的url发送post请求
	 * 
	 * @param url
	 * @param postData
	 * @return
	 * @throws IOException
	 */
	public static String post(String url, Map<String, Object> postData) throws IOException {

		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		OkHttpClient client = new OkHttpClient();
		RequestBody body = RequestBody.create(JSON, "");
		Request request = new Request.Builder().url(url).post(body).build();
		Response response = null;
		try {
			response = client.newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 对指定的url发送get请求
	 * 
	 * @param url
	 * @param getData
	 * @return
	 */
	public static String get(String url) {

		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(url).build();

		Response response = null;
		try {

			response = client.newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
