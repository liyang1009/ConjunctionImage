package com.game.app.mapper;

import org.apache.ibatis.annotations.Param;

import com.game.app.model.UserModel;

/**
 * user 表操作映射
 * 
 * @author liyang
 *
 */
public interface UserMapper {

	/**
	 * 获取单个实体
	 */
	public String selectItem();

	/**
	 * 增加用户游戏得分记录
	 * 
	 * @param recordData
	 */
	public void insertUser(UserModel userModel);

	/**
	 * 修改用户信息
	 * 
	 * @param userModel
	 */
	public void updateUser(UserModel userModel);

	/**
	 * 根据openid获取用户信息
	 * 
	 * @param opendid
	 * @return
	 */
	public UserModel selectUser(@Param("opendid") String opendid);

}
