package com.game.app.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * Game 表操作映射
 * 
 * @author liyang
 *
 */
public interface GamePictureMapper {

	/**
	 * 获取单个实体
	 */
	public List<String> selectPictrueByStep(@Param("raceStep") int raceStep, @Param("rankStep") int rankStep,
			@Param("mode") int mode);

	/**
	 * 根据游戏模式获取游戏的关卡数局级数
	 * 
	 * @param gameMode
	 * @return
	 */
	public List<Map<String, Object>> gameRankConfig();
}
