package com.game.app.mapper;

import java.util.Map;

/**
 * Game 表操作映射
 * 
 * @author liyang
 *
 */
public interface GameMapper {

	/**
	 * 获取单个实体
	 */
	public String selectItem();

	/**
	 * 增加用户游戏得分记录
	 * 
	 * @param recordData
	 */
	public void insertScore(Map<String, Object> recordData);

}
