package com.game.app.model;

public class UserModel {
	/**
	 * 微信openid
	 */
	private String openid;
	/**
	 * 获取微信数据所需的sessionkey
	 */
	private String sessionkey;
	/**
	 * 用户头像
	 */
	private String avatar;

	/**
	 * 注册时间
	 */
	private String registerTime;

	public String getOpenId() {
		return openid;
	}

	public void setOpenId(String openid) {
		this.openid = openid;
	}

	public String getSessionkey() {
		return sessionkey;
	}

	public void setSessionkey(String sessionkey) {
		this.sessionkey = sessionkey;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

}
