package com.game.app.dao;

import org.apache.ibatis.session.SqlSession;

import com.game.app.Application;
import com.game.app.mapper.UserMapper;
import com.game.app.model.UserModel;

/**
 * 用户数据处理相关类
 * 
 * @author liyang
 *
 */
public class UserDao {

	/**
	 * 根据open获取用户
	 * 
	 * @param openid
	 * @return
	 */
	public UserModel getUser(String openid) {
		SqlSession session = Application.openSession();
		UserModel user = null;
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			user = mapper.selectUser(openid);
		} finally {
			session.close();
		}
		return user;
	}

	/**
	 * 用户注册
	 * 
	 * @param recordData
	 */
	public void registerUser(UserModel user) {
		SqlSession session = Application.openSession(true);
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.insertUser(user);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * 修改用户
	 * 
	 * @param user
	 */
	public void modifyUser(UserModel user) {
		SqlSession session = Application.openSession(true);
		try {
			UserMapper mapper = session.getMapper(UserMapper.class);
			mapper.updateUser(user);
			session.commit();
		} finally {
			session.close();
		}
	}

}
