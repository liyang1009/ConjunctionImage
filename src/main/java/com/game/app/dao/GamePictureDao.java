package com.game.app.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.game.app.Application;
import com.game.app.mapper.GamePictureMapper;

/**
 * dao 游戏数据处理相关类
 * 
 * @author liyang
 *
 */
public class GamePictureDao {

	public List<String> selectPictruesByStep(int raceStep, int rankStep, int mode) {
		SqlSession session = Application.openSession();
		List<String> picList = null;
		try {
			GamePictureMapper mapper = session.getMapper(GamePictureMapper.class);
			picList = mapper.selectPictrueByStep(raceStep, rankStep, mode);
		} finally {
			session.close();
		}
		return picList;
	}

	public List<Map<String, Object>> selectGameRankByMode() {
		SqlSession session = Application.openSession();
		List<Map<String, Object>> ranks = null;
		try {
			GamePictureMapper mapper = session.getMapper(GamePictureMapper.class);
			ranks = mapper.gameRankConfig();
		} finally {
			session.close();
		}
		return ranks;
	}
}
