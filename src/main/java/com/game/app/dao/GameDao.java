package com.game.app.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.game.app.Application;
import com.game.app.mapper.GameMapper;

/**
 * dao 游戏数据处理相关类
 * 
 * @author liyang
 *
 */
public class GameDao {

	public String getItem() {
		SqlSession session = Application.openSession();
		String nameString = null;
		try {
			GameMapper mapper = session.getMapper(GameMapper.class);
			nameString = mapper.selectItem();
		} finally {
			session.close();
		}
		return nameString;
	}

	/**
	 * 记录游戏得分
	 * 
	 * @param recordData
	 */
	public void insertScore(Map<String, Object> recordData) {
		SqlSession session = Application.openSession();
		try {
			GameMapper mapper = session.getMapper(GameMapper.class);
			mapper.insertScore(recordData);
			session.commit();
		} finally {
			session.close();
		}
	}

}
